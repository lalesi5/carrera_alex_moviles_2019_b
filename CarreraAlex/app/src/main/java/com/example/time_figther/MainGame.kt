package com.example.time_figther

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.media.session.PlaybackStateCompat
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver



class MainGame : Fragment() {

    internal lateinit var welcomMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button

    @State
    var score=0

    @State
    var gameStarted = false

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long =50
    private lateinit var countDownTimer: CountDownTimer // para implementar la varibale timer

    @State
    var timeLeft = 10;



    private val args: MainGameArgs by navArgs()




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        StateSaver.restoreInstanceState(this,savedInstanceState)

        gameScoreTextView= view.findViewById(R.id.score)
        goButton = view.findViewById(R.id.go_button)
        timeLeftTextView = view.findViewById(R.id.time_left)


        gameScoreTextView.text =getString(R.string.score_i,score)
        //timeLeftTextView.text =getString(R.string.time_left_i, timeLeft)

        welcomMessage= view.findViewById(R.id.welcome_message)
        val playerName = args.playerName




        goButton.setOnClickListener { incrementScore()}
        resetGame()

        if(gameStarted){
            resetGame()
            return
        }

    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this,outState)
        countDownTimer.cancel()

    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }


    companion object {

        private val SCORE_KEY="SCORE"
    }

    fun incrementScore(){


        if(!gameStarted){
            countDownTimer.start()

        }

        score ++
        score*100
        gameScoreTextView.text = getString(R.string.score_i, score)
    }

    private fun resetGame(){
        score= 0
        gameScoreTextView.text =getString(R.string.score_i,score)

        timeLeft = 10
        timeLeftTextView.text =getString(R.string.time_left_i,timeLeft)

        countDownTimer= object :CountDownTimer(initialCountDown,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text =getString(R.string.time_left_i, timeLeft)
            }

            override fun onFinish() {
                endGame()

            }

        }
    }

    private fun startGame(){

        countDownTimer.start()
        gameStarted = true

    }

    private fun restoreGame(){
        gameScoreTextView.text =getString(R.string.score_i,score)

        countDownTimer= object :CountDownTimer
            (timeLeft * 100L,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 100
                timeLeftTextView.text =getString(R.string.time_left_i, timeLeft)
            }

            override fun onFinish() {
                endGame()

            }

        }
        countDownTimer.start()
    }

    private fun endGame(){
        Toast.makeText(
            activity,
            getString(R.string.end_game, score),
            Toast.LENGTH_LONG).show()

        resetGame()

    }


}
